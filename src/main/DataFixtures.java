package main;


import main.domain.article.Article;
import main.domain.article.ArticleRepository;
import main.domain.board.Board;
import main.domain.board.BoardRepository;
import main.domain.user.User;
import main.domain.user.UserRepository;

/**
 * Created with IntelliJ IDEA.
 * User: Coupang
 * Date: 2014. 6. 27.
 * Time: 오후 9:44
 * To change this template use File | Settings | File Templates.
 */
public class DataFixtures {

    UserRepository userRepository = UserRepository.getInstance();
    BoardRepository boardRepository = BoardRepository.getInstance();
    ArticleRepository articleRepository = ArticleRepository.getInstance();

    public void makeUserData() {
        User user1 = new User("hi", "1234");
        userRepository.save(user1);

        User user2 = new User("admin", "3456");
        userRepository.save(user2);
    }

    public void makeBoardData() {
        Board board1 = new Board("공지사항");
        boardRepository.save(board1);

        Board board2 = new Board("자유게시판");
        boardRepository.save(board2);
    }

    public void makeFreeBoardArticleData() {
        Article article1 = new Article("자유게시판", "hi", "hi", "nice to meet you.");
        Article article2 = new Article("자유게시판", "hi", "bye", "I am full");
        Article article3 = new Article("자유게시판", "admin", "good", "I am happy");

        articleRepository.save(article1);
        articleRepository.save(article2);
        articleRepository.save(article3);
    }

    public void makeNoticeBoardArticleData() {
        Article article1 = new Article("공지사항", "admin", "hi", "nice to meet you.");
        Article article2 = new Article("공지사항", "admin", "bye", "I am full");
        Article article3 = new Article("공지사항", "admin", "good", "I am happy");

        articleRepository.save(article1);
        articleRepository.save(article2);
        articleRepository.save(article3);
    }
}
