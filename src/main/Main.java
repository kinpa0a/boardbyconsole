package main;

import main.domain.user.User;
import main.handler.input.InputHandler;
import main.handler.logic.LogicHandler;

/**
 * Created by Coupang on 2014. 6. 28..
 */

public class Main {

//        DB를 사용하지 않지만 데이터는 저장되어야 한다.
//        로그인 구현 : id password
//        공지사항/자유게시판/새로생성된게시판..
//        게시판생성-게시판이름 생성하면 추가됨
//        게시판 들어가면 해당 게시판에 있는 글 리스트가 쭉
//        페이징은 괜찮지만 정렬은 구현해보세요(번호, 제목, 작성자)
//        글쓰기 제목 내용 추가 읽기/수정 포함
    //TODO extends/implements 가 하나도 없다. ㅠ,ㅠ..

    //currentUser가 한 명이라고 가정하고 static으로 선언.
    //TODO 멀티유저 환경에서는 어떻게 해야할까?
    public static User currentUser = null;

    public static void main(String[] args) {
        setDataFixtures();

        LogicHandler logicHandler = new LogicHandler();
        logicHandler.login(InputHandler.inputUserInfo());
    }

    public static void setDataFixtures() {
        DataFixtures dataFixtures = new DataFixtures();
        dataFixtures.makeUserData();
        dataFixtures.makeBoardData();
        dataFixtures.makeFreeBoardArticleData();
        dataFixtures.makeNoticeBoardArticleData();
    }
}