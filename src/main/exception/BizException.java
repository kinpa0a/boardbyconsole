package main.exception;

/**
 * Created by Coupang on 2014. 6. 26..
 */
public class BizException extends Exception{

    private String msg;

    public BizException(String msg) {
        this.msg = msg;
    }

    @Override
    public String getMessage() {
        return msg;
    }
}
