package main.exception;

/**
 * Created by Coupang on 2014. 6. 27..
 */
public class Messages{

    public static final String NOT_VALID_ID = "유효하지 않은 유저 아이디 입니다.";
    public static final String NOT_VALID_PASSWORD = "유효하지 않은 패스워드입니다.";
    public static final String DUPLICATED_ID = "이미 존재하는 유저 아이디 입니다.";
    public static final String LOGIN_FAILURE = "로그인에 3번 실패하셨습니다.";

}
