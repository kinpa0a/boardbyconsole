package main.util;

import main.domain.user.User;
import main.domain.user.UserRepository;
import main.exception.BizException;
import main.exception.Messages;

/**
 * Created by Coupang on 2014. 6. 28..
 */
public class Validator {

    public static void validateUser(String id, String password) throws BizException {
        User user = UserRepository.getInstance().findByID(id);
        if(user == null) {
            throw new BizException(Messages.NOT_VALID_ID);
            //TODO password == UserList.get(id) 가 왜 false가 나올까?
        } else if(!password.equals(user.getPassword())) {
            throw new BizException(Messages.NOT_VALID_PASSWORD);
        }
    }
}
