package main.domain.article;

import main.domain.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Coupang on 2014. 6. 27..
 */
public class ArticleRepository implements Repository<Article> {

    private static ArticleRepository articleRepository = null;

    private ArticleRepository() {}

    public static synchronized ArticleRepository getInstance() {
        if(articleRepository == null) {
            articleRepository = new ArticleRepository();
        }
        return articleRepository;
    }

    private static List<Article> articleList = new ArrayList();

    public List<Article> findAll() {
        return articleList;
    }

    @Override
    public Article delete(Integer index) {
        if(index == null) return null;
        for(Article article : articleList) {
            if(index.equals(article.getIndex())) {
                articleList.remove(article);
                return article;
            }
        }
        return null;
    }

    public List<Article> delete(List<Article> articles) {
        if(articles == null) return null;
        for(Article article : articles) {
            articleRepository.delete(article.getIndex());
        }
        return articles;
    }

    public Article find(Integer index) {
        if(index == null) return null;
        for(Article article :articleList) {
            if(index.equals(article.getIndex())) return article;
        }
        return null;
    }

    public Article save(Article article) {
        //TODO 여기도 exception을 처리해줄 것인가?
        articleList.add(article);
        return article;
    }
}
