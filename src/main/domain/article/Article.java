package main.domain.article;

/**
 * Created by Coupang on 2014. 6. 27..
 */
public class Article {

    //TODO Board가 articleList를, Article이 boardName을 갖고 있는게 뭔가 정보가 중첩되는 느낌이다.
    private int index;
    private String userId;
    private String title;
    private String contents;
    private String boardName;
    private static int counter;

    public Article(String boardName, String userId, String title, String contents) {
        this.userId = userId;
        this.title = title;
        this.contents = contents;
        this.boardName = boardName;
        this.index = ++counter;
    }

    public String getUserId() {
        return userId;
    }

    public String getTitle() {
        return title;
    }

    public String getContents() {
        return contents;
    }

    public String getBoardName() {
        return boardName;
    }

    public Integer getIndex() {
        return index;
    }


}
