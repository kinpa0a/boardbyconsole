package main.domain.article;

import java.util.Comparator;

/**
 * Created by Coupang on 2014. 7. 1..
 */
public enum ArticleComparator implements Comparator<Article> {
    INDEX("i"){
        @Override
        public int compare(Article o1, Article o2) {
            return compareToNull(o1.getIndex(), o2.getIndex());
        }
    },
    TITLE("t"){
        @Override
        public int compare(Article o1, Article o2) {
            return compareToNull(o1.getTitle(), o2.getTitle());
        }
    } ,
    WRITER("w"){
        @Override
        public int compare(Article o1, Article o2) {
            return compareToNull(o1.getUserId(), o2.getUserId());

        }
    };

    private static <T extends Comparable> int compareToNull(final T o1, final T o2) {
        if(o1 == null) {
            if(o2 == null) return 0;
            else return -1;
        }
        if(o2 == null) return 1;
        return o1.compareTo(o2);
    }

    private String sortOption;

    private ArticleComparator(String sortOption) {
        this.sortOption = sortOption;
    };

    public String getSortOption() {
        return sortOption;
    }
}
