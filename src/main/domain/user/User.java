package main.domain.user;

import main.domain.article.Article;
import main.domain.article.ArticleRepository;

import java.util.ArrayList;
import java.util.List;

/**
* Created by Coupang on 2014. 6. 26..
*/
public class User {
    private int index;
    private String id;
    private String password;
    private List<Article> articleList;
    private static int counter;

    public User(String id, String password) {
        this.id = id;
        this.password = password;
        this.index = ++counter;
        this.articleList = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public int getIndex() {
        return index;
    }

    public List<Article> getArticleList() {
        List<Article> allArticleList = ArticleRepository.getInstance().findAll();
        for(Article article : allArticleList) {
            if(article.getUserId() == id && !articleList.contains(article)) {
                articleList.add(article);
            }
        }
        return articleList;
    }

//    public List<Article> getArticleList() {
//        if(articleList == null) {
//            articleList = new ArrayList();
//            setArticleList();
//        }
//        return articleList;
//    }
//
//    public void setArticleList() {
//        List<Article> allArticleList = ArticleRepository.getInstance().findAll();
//        for (Article article : allArticleList) {
//            if (article.getUserId() == id) {
//                articleList.add(article);
//            }
//        }
//    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (!id.equals(user.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
