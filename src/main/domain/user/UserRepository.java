package main.domain.user;

import main.domain.Repository;
import main.domain.article.Article;
import main.domain.article.ArticleRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Coupang
 * Date: 2014. 6. 27.
 * Time: 오후 9:31
 * To change this template use File | Settings | File Templates.
 */
public class UserRepository implements Repository<User> {

    private static UserRepository userRepository = null;
    private static ArticleRepository articleRepository = ArticleRepository.getInstance();

    private UserRepository() {}

    public static synchronized UserRepository getInstance() {
        if(userRepository == null) {
            userRepository = new UserRepository();
        }
        return userRepository;
    }
    
    private static List<User> userList = new ArrayList();

    public User save(User user) {
        //TODO 이걸 구현해야하나? Exception을 던지면 픽스처 만들 때 처리가 이상해진다. ㅠ,ㅠ...
//        if(UserList.containsKey(id)) {
//            throw new BizException(ExceptionMessages.DUPLICATED_ID);
//        } else {
            userList.add(user);
//        }
        return user;
    }

    public User find(Integer index) {
        if(index == null) return null;
        for(User u :userList) {
            if(index.equals(u.getIndex())) return u;
        }
        return null;
    }

    public User findByID(String id) {
        if(id == null) return null;
        for(User u :userList) {
            if(id.equals(u.getId())) return u;
        }
        return null;
    }

    public List<User> findAll() {
        return userList;
    }

    @Override
    public User delete(Integer index) {
        if(index == null) return null;
        for(User user : userList) {
            if(index.equals(user.getIndex())) {
                articleRepository.delete(user.getArticleList());
                userList.remove(user);
                return user;
            }
        }
        return null;
    }
}
