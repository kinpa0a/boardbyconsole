package main.domain.board;


import main.domain.article.Article;
import main.domain.article.ArticleRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Coupang on 2014. 6. 27..
 */
public class Board {

    private int index;
    private String boardName;
    private List<Article> articleList;
    private static int counter;

    public Board(String boardName) {
        this.boardName = boardName;
        this.index = ++counter;
        this.articleList = new ArrayList();
    }

    public String getBoardName() {
        return boardName;
    }

    public int getIndex() {
        return index;
    }

    //TODO 전체 리스트에서 해당 리스트를 뽑아오는 작업과 아예 리스트를 들고 있는 것 중 무엇이 나은가?
    //FIXME 일단 임시로..그냥..새로 생성해본다..잠시 여기까지만..
    public List<Article> getArticleList() {
        List<Article> allArticleList = ArticleRepository.getInstance().findAll();
        articleList = new ArrayList();
        for(Article article : allArticleList) {
            if(article.getBoardName() == boardName && !articleList.contains(article)) {
                articleList.add(article);
            }
        }
        return articleList;
    }

//    public List<Article> getArticleList() {
//        if(articleList == null) {
//            articleList = new ArrayList();
//            setArticleList();
//        }
//        return articleList;
//    }
//
//    public void setArticleList() {
//        List<Article> allArticleList = ArticleRepository.getInstance().findAll();
//        for (Article article : allArticleList) {
//            if (article.getBoardName() == boardName) {
//                articleList.add(article);
//            }
//        }
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Board)) return false;

        Board board = (Board) o;

        if (index != board.index) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return index;
    }
}
