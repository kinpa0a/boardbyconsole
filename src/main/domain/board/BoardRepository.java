package main.domain.board;

import main.domain.Repository;
import main.domain.article.Article;
import main.domain.article.ArticleRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Coupang on 2014. 6. 27..
 */
public class BoardRepository implements Repository<Board> {

    private static BoardRepository boardRepository = null;
    private ArticleRepository articleRepository = ArticleRepository.getInstance();

    private BoardRepository() {}

    public static synchronized BoardRepository getInstance() {
        if(boardRepository == null) {
            boardRepository = new BoardRepository();
        }
        return boardRepository;
    }

    private static List<Board> boardList = new ArrayList();

    public List<Board> findAll() {
        return boardList;
    }

    public Board find(Integer index) {
        if(index == null) return null;
        for(Board b :boardList) {
            if(index.equals(b)) return b;
        }
        return null;
    }

    public Board save(Board board) {
        //TODO 여기도 exception을 처리해줄 것인가?
        boardList.add(board);
        return board;
    }

    @Override
    public Board delete(Integer index) {
        if(index == null) return null;
        for(Board board : boardList) {
            if(index.equals(board.getIndex())) {
                articleRepository.delete(board.getArticleList());
                boardList.remove(board);
                return board;
            }
        }
        return null;
    }


}
