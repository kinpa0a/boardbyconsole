package main.handler.logic;

import main.Main;
import main.domain.article.Article;
import main.domain.article.ArticleComparator;
import main.domain.article.ArticleRepository;
import main.domain.board.Board;
import main.domain.board.BoardRepository;
import main.domain.user.User;
import main.exception.BizException;
import main.exception.Messages;
import main.handler.input.InputHandler;
import main.util.Validator;

import java.util.Collections;
import java.util.List;

public class LogicHandler {

    BoardRepository boardRepository = BoardRepository.getInstance();
    ArticleRepository articleRepository = ArticleRepository.getInstance();

    public void login(User currentUser) {
        login(currentUser, 0);
    }

    public void login(User currentUser, int loginFailureCount) {
        try {
            Validator.validateUser(currentUser.getId(), currentUser.getPassword());
            Main.currentUser = currentUser;
            System.out.println("로그인에 성공했습니다.");
            showBoardList();
        } catch (BizException e) {
            System.err.println(e.getMessage());
            loginFailureCount++;
            if(loginFailureCount >= 3) {
                System.err.println(Messages.LOGIN_FAILURE);
                System.exit(0);
            } else {
                //TODO 여기서 인풋을 받아도 되는건가? 뭔가 찝찝하다.
                Main.currentUser = InputHandler.inputUserInfo();
                login(Main.currentUser, loginFailureCount);
            }
        }
    }

    public void showBoardList() {
        List<Board> boardList = boardRepository.findAll();

        for(Board board : boardList) {
            System.out.println((boardList.indexOf(board)+1) +". " + board.getBoardName());
        }

        System.out.println("들어가실 게시판 번호를 입력해주세요.");
        System.out.println("게시판을 추가하시려면 0을 입력해주세요.");
        System.out.println("프로그램을 종료하시려면 #을 입력해주세요.");

        chooseBoardListOption(InputHandler.getInput());
    }

    public void showArticleList(Board board) {
        showArticleList(board, ArticleComparator.INDEX);
    }

    public void showArticleList(Board board, ArticleComparator articleComparator) {
        List<Article> articleList = board.getArticleList();
        Collections.sort(articleList, articleComparator);
        for(Article article : articleList) {
            System.out.println((articleList.indexOf(article)+1) + ") 제목: " + article.getTitle() + " 작성자: " + article.getUserId());
        }

        System.out.println("읽으실 글 번호를 입력해주세요.");
        System.out.println("인덱스 기준으로 정렬하시려면 i, 제목은 t, 작성자는 w를 입력해주세요.");
        System.out.println("게시물을 추가하시려면 0을 입력해주세요.");
        System.out.println("게시판을 삭제하시려면 &을 입력해주세요.");
        System.out.println("이전 메뉴를 보시려면 *을 입력해주세요.");
        System.out.println("프로그램을 종료하시려면 #을 입력해주세요.");

        chooseArticleListOption(board, InputHandler.getInput());
    }

    public void showArticle(Board board, Article article) {
        System.out.println("제목: " + article.getTitle());
        System.out.println("글쓴이: " + article.getUserId());
        System.out.println("내용: " + article.getContents());

        System.out.println("게시글을 삭제하시려면 &을 입력해주세요.");
        System.out.println("이전 메뉴를 보시려면 *을 입력해주세요.");
        System.out.println("프로그램을 종료하시려면 #을 입력해주세요.");

        chooseArticleOption(board, article, InputHandler.getInput());
    }

    public void chooseBoardListOption(String boardListOption) {
        if (boardListOption.equals(Option.NEW.getMenu())) {
            newBoard();
            showBoardList();
            chooseBoardListOption(InputHandler.getInput());
        } else if (boardListOption.equals(Option.EXIT.getMenu())) {
            System.exit(0);
        } else {
            Board board = boardRepository.findAll().get(Integer.parseInt(boardListOption) - 1);
            showArticleList(board);
        }
    }

    public void chooseArticleListOption(Board board, String articleListOption) {
        if (articleListOption.equals(Option.NEW.getMenu())) {
            newArticle(board);
            showArticleList(board);
            chooseArticleListOption(board, InputHandler.getInput());
        } else if (articleListOption.equals(Option.PREVIOUS.getMenu())) {
            showBoardList();
        } else if (articleListOption.equals(ArticleComparator.INDEX.getSortOption())) {
            showArticleList(board, ArticleComparator.INDEX);
        } else if (articleListOption.equals(ArticleComparator.TITLE.getSortOption())) {
            showArticleList(board, ArticleComparator.TITLE);
        } else if (articleListOption.equals(ArticleComparator.WRITER.getSortOption())) {
            showArticleList(board, ArticleComparator.WRITER);
        } else if (articleListOption.equals(Option.DELETE.getMenu())) {
            boardRepository.delete(board.getIndex());
            showBoardList();
        } else if (articleListOption.equals(Option.EXIT.getMenu())) {
            System.exit(0);
        } else {
            Article article = board.getArticleList().get(Integer.parseInt(articleListOption) - 1);
            showArticle(board, article);
        }
    }

    public void chooseArticleOption(Board board, Article article, String articleOption) {
        if (articleOption.equals(Option.PREVIOUS.getMenu())) {
            showArticleList(board);
        } else if (articleOption.equals(Option.DELETE.getMenu())) {
            articleRepository.delete(article.getIndex());
            showArticleList(board);
        } else if (articleOption.equals(Option.EXIT.getMenu())) {
            System.exit(0);
        }
    }

    public void newBoard() {
        System.out.println("생성하실 게시판 이름을 입력해주세요.");
        Board board = new Board(InputHandler.getInput());
        boardRepository.save(board);
        System.out.println("게시판 생성이 완료되었습니다.");
    }

    public void newArticle(Board board) {
        System.out.println("게시물 제목을 입력해주세요.");
        String articleTitle = InputHandler.getInput();
        System.out.println("게시물 내용을 입력해주세요.");
        String articleContents = InputHandler.getInput();
        
        Article article = new Article(board.getBoardName(), Main.currentUser.getId(), articleTitle, articleContents);
        articleRepository.save(article);
        System.out.println("게시물 생성이 완료되었습니다.");
    }
}