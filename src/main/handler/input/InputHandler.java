package main.handler.input;

import main.domain.user.User;

import java.util.Scanner;

/**
* Created by Coupang on 2014. 6. 28..
*/
public class InputHandler {

    public static Scanner scan = new Scanner(System.in);

    public static String getInput() {
        return scan.nextLine();
    }

    public static User inputUserInfo() {
        System.out.println("ID를 입력하세요: ");
        String id = getInput();

        System.out.println("비밀번호를 입력하세요: ");
        String password = getInput();

        return new User(id, password);
    }

}
